module.exports = {
    baseUrl: '/progressive-weather-app/',
    pwa: {
        themeColor: '#6CB9C8',
        msTileColor: '#484F60'
    }
}
module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/' + process.env.codr-devops-hw + '/'
    : '/'
}
